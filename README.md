# voltages.me

Personal website for my projects and about myself.

I have rewritten this multiple times now. Even made a seperate branch for a older version made using Bulma as the CSS framework.

## Credits

* [Feathericons](https://github.com/feathericons/feather)
    * Well done open-source icon set used in my website
* [Simpleicons](https://simpleicons.org/)
    * Giant list of popular brand logos in SVG form 
